---
title: 'Introduction to R'
subtitle: 'Exercise (Part II)'
output:
  html_document: default
  pdf_document: default
---

```{r options, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo=TRUE, eval=TRUE,
                      cache = FALSE)
```

In this practical exercise you will apply your newly acquired R skills for data analysis and visualisation.

## 1. Data analysis

**1. Load the dataset which you created in the first part of the exercise.**

```{r 1-1}
d <- read.csv('btw2013.csv', stringsAsFactors = F)
```

**2. Correlate turnout and unemployment rate.**

```{r 1-2}
cor(d$turnout, d$unemployment)
```


**3. Conduct difference-in-means tests between east and west for the variables turnout and unemployment rate.**

```{r 1-3}
t.test(turnout ~ east, d)
t.test(unemployment ~ east, d)
```

**4. Regress the turnout rate on the unemployment rate. Save the model in an object.**

```{r 1-4}
m1 <- lm(turnout ~ unemployment, d)
```

**5. Add two to three control variables iteratively. Save** 

```{r 1-5}
m2 <- lm(turnout ~ unemployment + popdensity, d)
m3 <- lm(turnout ~ unemployment + popdensity + abitur, d)
```

**6. Present the results in a regression table**

```{r 1-6latex, results='asis', include=knitr::is_latex_output()}
library(texreg)
texreg(list(m1, m2, m3),
       custom.coef.names = c(NA, 'Unemployment', 'Populaton Density', 
                             'Abitur'),
       reorder.coef = c(2:4, 1))
```

```{r 1-6html, include=knitr::is_html_output()}
htmlreg(list(m1, m2, m3),
       custom.coef.names = c(NA, 'Unemployment', 'Populaton Density', 
                             'Abitur'),
       reorder.coef = c(2:4, 1))
```

**7. Test whether the partial correlation between turnout and unemployment is different between east and west.**

```{r 1-7}
summary(m_i <- lm(turnout ~ unemployment * east, d))
```

## 2. Data and model visualization

**1. Plot the number of districsts per state.**

```{r 2-1}
library(ggplot2)
ggplot(d, aes(x = land)) + geom_bar()
```


**2. Plot turnout by state.**

```{r 2-2}
library(dplyr)

df <- d %>% group_by(land) %>% summarise(turnout = mean(turnout))
df
ggplot(df, aes(x = land, y = turnout)) + geom_bar(stat = 'identity')
```

**3. Sort the bars by turnout.**

```{r 2-3}
ggplot(df, aes(x = reorder(land, turnout), y = turnout)) + 
  geom_bar(stat = 'identity')
```

**4. Highlight East-German states.**

```{r 2-4}
df <- d %>% group_by(land) %>% summarise(turnout = mean(turnout),
                                         east = unique(east))
ggplot(df, aes(x = reorder(land, turnout), y = turnout, fill = east)) + 
  geom_bar(stat = 'identity')
```

**5. Label the axis and make the legend easier to read. Also, fix the issue with overlapping state names.**

```{r 2-5}
f <- ggplot(df, aes(x = reorder(land, turnout), y = turnout, fill = east)) + 
  geom_bar(stat = 'identity') + ylab('Turnout (%)') + xlab('') +
  scale_fill_discrete(name = '', labels = c('West', 'East'))

f + coord_flip()
```

**6. Ok, the differences in turnout are not that clear. Add numbers on top of the bars to indicate the mean turnout in states.**

```{r 2-6}
f <- f + geom_text(aes(y = turnout +2, label = round(turnout, 0))) + coord_flip()
f
```


**7. Finally, apply `theme_tufte()` to make the plot look even nicer.**

```{r 2-7}
library(ggthemes)

f + theme_tufte()
```

**8. Draw a scatterplot of turnout against the unemployment rate.**

```{r 2-8}
ggplot(d, aes(x = unemployment, y = turnout)) + geom_point()
```

**9. Add the size of districts as information in the plot.**

```{r 2-9}
g <- ggplot(d, aes(x = unemployment, y = turnout)) 
g + geom_point(aes(color = population))
g + geom_point(aes(size = population))
g2 <- g + geom_point(aes(size = population, color = population))
```

**10. Add linear fit line as solid black line. No cofindence interval.**

```{r 2-10}
g3 <- g2 + geom_smooth(method = 'lm', se = F, color = 'black')
g3
```

**11. Add a LOES curve (without confidence interval) to the plot. Make it dashed so it is easy to distinguish from the linear fit line.**

```{r 2-11}
g3 + geom_smooth(se = F, color = 'black', linetype = 'dashed')
```

**12. Return to the simple scatterplot. Now highlight the different Länder.**

```{r 2-12}
ggplot(d, aes(x = unemployment, y = turnout, color = land)) + geom_point()
```