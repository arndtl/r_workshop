---
title: "Introduction to R"
subtitle: "Data wrangling"
output:
  xaringan::moon_reader:
    css:
      - default
      - css/lexis.css
      - css/lexis-fonts.css
      - css/mystyles.css
    lib_dir: libs
    seal: false
    self_contained: true
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
      slideNumberFormat: "%current%"
      ratio: "16:9"
    includes:
      after_body: "js/collapseoutput.js"
---
layout: true

<style>
    .onehundredtwenty {
        font-size: 120%;
    }

.ninety {
    font-size: 90%;
}

.eightyfive {
    font-size: 85%;
}

.eighty {
    font-size: 80%;
}

.seventyfive {
    font-size: 75%;
}

.seventy {
    font-size: 70%;
}

.fifty {
    font-size: 50%;
}

.forty {
    font-size: 40%;
}
</style>


```{r meta, echo=FALSE, warning = F, message=F}
library(metathis)
meta() %>%
    meta_general(
        description = "R Bootcamp",
        generator = "xaringan and remark.js"
    )
# %>%
#   # meta_name("github-repo" = "favstats/xxx") %>%
#   meta_social(
#     title = "Intro to R Programming",
#     # url = "https://www.favstats.eu",
#     # og_type = "website",
#     og_author = "Jan Panhuysen / Arndt Leininger",
#     twitter_card_type = "summary_large_image",
#     twitter_creator = "@favstats"
#   )
```

```{r setup, include=FALSE}
# dateWritten <- format(as.Date('2020-05-04'), format="%B %d %Y")
workshop_day <- format(as.Date("2022-09-01"), format="%B %Y")
pacman::p_load(tidyverse, fontawesome, tidyverse, knitr, rstatsmemes)

options(
    htmltools.dir.version = FALSE,
    knitr.table.format = "html",
    knitr.kable.NA = ""
)
knitr::opts_chunk$set(
    warning = FALSE,
    message = FALSE,
    fig.path = "figs/",
    fig.width = 7.252,
    fig.height = 4,
    comment = "#>",
    fig.retina = 3 # Better figure resolution
)
# Enables the ability to show all slides in a tile overview by pressing "o"
xaringanExtra::use_tile_view()
xaringanExtra::use_panelset()
xaringanExtra::use_clipboard()
# xaringanExtra::use_share_again()
# xaringanExtra::style_share_again(share_buttons = "all")
xaringanExtra::use_extra_styles(
    hover_code_line = TRUE,
    mute_unhighlighted_code = FALSE
)
# xaringanExtra::use_webcam()


knitr::opts_chunk$set(warning = F, message = F, eval = F) # Whether to display errors
```

---
name: title-slide
class: primary, middle


# .eighty[`r rmarkdown::metadata$title`]

### .eighty[`r rmarkdown::metadata$subtitle`]

.seventy[
Arndt Leininger

[`r fa(name = "twitter", fill = "black")` @a_leininger](http://twitter.com/a_leininger)<br>
[`r fa(name = "link", fill = "black")` aleininger.eu](https://www.aleininger.eu)
]

---
class: center, middle

# Data wrangling

---

## Data wrangling

- Remember that some say that quantitative social science research is **80%** data collection, cleaning and wrangling and only **20%** actual analysis.
- Data don't always come in nicely formatted files (e.g. .dta). Sometimes you have a data source or sources in files that aren’t set up for R to read and start analyzing right away.
- You often have to do a bit of work to get things into the format you want: the more of that work is recorded the better.
- `R` and the packages `dpylr` and `tidyr` amongst others help you reduce the time you spend wrangling and makes that time more fun

---

## tidyverse

- We will be using the package `dplyr` and maybe also `tidyr`
- Both packages are included along with other packages in the meta-package `tidyverse`
- `tidyverse` is already pre-installed in all projects in this course on Posit Cloud
- On your own computer, you will have to install the package first
    - The function for this is `install.packages()`, more on this later 

---

## dplyr

- `dplyr` is a package which simplifies data wrangling a lot
- It provides a number of functions which together allow a wide range of data manipulation
    - `arrange()`: sorting data
    - `filter()`: subsetting data 
    - `select()` (and `rename()`): subsetting data (by column)
    - `mutate()`: add or replace variables
    - `group_by()`: do a `mutate()` or `summarize()` group-wise
    - `summarize()`: aggregate data
- It provides 'piping' functionality

---

## Wrangling

Sort domestic cars by price

```{r readdta_hidden, eval=TRUE, echo=FALSE}
library(foreign)
df <- read.dta('../data/auto.dta')
```

.details[
```{r, eval=TRUE}
df %>% filter(foreign == 'Domestic') %>% arrange(price) %>% 
    select(make, foreign, price)
```
]

---

## Piping

- `%>%` is a 'piping' operator, it passes on output from a function to another, allowing you to write code in the order of execution
- The package `dpylr` provides piping functionality for R through the package `magrittr`

.details[
```{r pipe_example, eval=TRUE}
df %>% filter(price == max(price)) %>% select(make, price)
```
]

.details[
```{r pipe_example2, eval=TRUE}
# is easier to read than
select(filter(df, price == max(price)), make, price)
```
]

Keyboard shortcut <kbd>Ctrl</kbd> `+` <kbd>`r knitr::asis_output("\U21E7")`</kbd> `+` <kbd>M</kbd> produces the piping symbol

---

## Piping

.details[
```{r pipe_example3, tidy=FALSE, eval=TRUE}
"The pipe" %>%
  paste0(" can do this.") %>%
  paste0(., " And also this.") %>%
  paste0("The pipe is allmighty! ", .)
```
]

Read more about it at http://blog.revolutionanalytics.com/2014/07/magrittr-simplifying-r-code-with-pipes.html

<!--
## Wrangling

The average price of domestic cars is also simple to obtain

```{r subsetting2, eval=TRUE, tidy=FALSE}
df %>% filter(foreign == 'Domestic') %>% 
  summarise(average = mean(price))
```
-->

---

## Creating a new variable

Create a variable which indicates whether a car's price is above average

.details[
```{r wrangling3, eval=TRUE, tidy=FALSE}
tmp <- df %>%
  mutate(relativeprice = price - mean(price),
         expensive = relativeprice > 0)
tmp %>% select(price, relativeprice, expensive)
```
]

---

## Aggregating data

.details[
```{r summarize, eval=TRUE}
df %>% summarize(price = mean(price))
```
]

- `summarize()` in conjunction with `group_by()` can be used to aggregate data
- function used within `summarize()` must return a single value

.details[
```{r summarize2, eval=TRUE}
df %>% group_by(foreign) %>% 
    summarize(price = mean(price))
```
]

<!--
## A tibble

- `as_tibble()` turns a data.frame into a slightly modified data.frame (a 'tibble') with improved printing capabilities

```{r as_tibble, eval=TRUE}
df <- as_tibble(df)
df
```

## A tibble

- `as_tibble()` turns a data.frame into a slightly modified data.frame with improved printing capabilities

```{r as_tibble2, eval=TRUE}
df <- as_tibble(df)
print(df, n = 5)
```
-->

---

## `glimpse()`

.details[
```{r glimpse, eval=TRUE}
glimpse(df)
```
]

<!--
## Tidyverse

```{r tidyverse}
install.packages('tidyverse')
library(tidyverse)
```

- collection of useful R [packages](https://www.tidyverse.org/packages/) by [Hadley Wickham](http://hadley.nz/) (e.g. dplyr, tidyr, ggplot2, readr, stringr)
- https://www.tidyverse.org/
-->

---
class: center, middle

# .fancy[Hands-on-3-1]

---
class: middle

## .fancy[Hands-on-3-1]

Access "Hands-on-3-1" <!--in the space "Introduction to R" on Posit Cloud.-->

- `hands-on-3-1.R` contains the tasks, write your answers into the scripts
- If you're stuck, try whether you can fill in the blanks and fix the errors in `hands-on-3-1-partial.R`
- `hands-on-3-1-solutions.R` contains the full solutions

---
class: center, middle

# Merging and reshaping data

---

## Merging data

- `dplyr` provides great functions for merging data
- `left_join()` keeps all observations in `d1` and adds those in `d2` that can be matched
- Takes two data.frames as input
- Identifier variable names can differ

```{r merge}
d1 <- read.csv('data/d1.csv')
d2 <- read.csv('data/d2.csv')
m <- left_join(d1, d2, by = c('nation' = 'country', "year"))
```

- there are also: `inner_join`, `right_join`, `semi_join`,
  `anti_join`, `full_join`

---

## Merging data

<img src="img/dplyr-data-join-functions-overview_inner_join-left_join-right_join-full_join-semi_join-anti_join.png" style="width:70%" />

https://statisticsglobe.com/r-dplyr-join-inner-left-right-full-semi-anti

---
class: center, middle

# .fancy[Hands-on-3-2]

---

## .fancy[Hands-on-3-2]

Access "Hands-on-3-2" <!--in the space "Introduction to R" on Posit Cloud.-->

- `hands-on-3-2.R` contains the tasks, write your answers into the scripts
- If you're stuck, try whether you can fill in the blanks and fix the errors `hands-on-3-2-partial.R`
- `hands-on-3-2-solutions.R` contains the full solutions

---
class: center, middle

# Appendix

---

## Tidying data

- Tidy data refers to data in which each row is an observation and each column a variable
- This is also known as 'long' format as opposed to 'wide' format
- Real-world data do not always come in this form
- The package `tidyr` provides functions to 'clean up' data

```{r, echo=FALSE, eval=TRUE}
library(tidyr)
```

```{r}
install.packages('tidyr')
library(tidyr)
```

---

## Reshaping data

- Let's use a very simple made-up `data.frame` 

```{r messy, eval=TRUE}
messy <- data.frame(
  name = c("Wilbur", "Petunia", "Gregory"),
  a = c(67, 80, 64),
  b = c(56, 90, 50)
)

messy
```

- Imagine this is data from a clinical trial. We have three variables, patient name, drug and heartrate, but only one is a variable yet
- Based on https://blog.rstudio.org/2014/07/22/introducing-tidyr/

---

## Reshaping data

- We will use `tidyr`'s `pivot_longer()` to gather the a and b columns into key-value pairs of `drug` and `heartrate`

```{r gather, eval=TRUE, tidy=F}
tidy <- messy %>% pivot_longer(a:b, names_to = 'drug', 
                               values_to = 'heartrate')

tidy
```

---

## And back to wide format

```{r spread, eval=TRUE}
pivot_wider(tidy, names_from = drug, values_from = heartrate)
```