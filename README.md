# R Workshop

**Upcoming workshops**: 

5--6 October 2024, Hertie School, Berlin

**Past workshops**:

28--29 October 2024, Hertie School, Berlin

11--12 February 2023, Hertie School, Berlin

8--9 October 2022, Hertie School, Berlin

12--13 Febuary 2022, Hertie School, Berlin

18--19 September 2021, Hertie School, Berlin

19--20 and 26--27 September 2020, Hertie School, Berlin

8--9 February 2020, Hertie School of Governance, Berlin

21--22 and 28--29 September 2019, Hertie School of Governance, Berlin

09--10 February 2019, Hertie School of Governance, Berlin

10--11 December 2018, Wirtschaftsuniversität Wien

17--18 November 2018, Hertie School of Governance, Berlin

03--04 November 2018, Hertie School of Governance, Berlin

23--24 April 2018, Wirtschaftsuniversität Wien

17--18 and 24--25 February 2018, Hertie School of Governance, Berlin

24 November 2017, Wissenschaftszentrum Berlin für Sozialforschung

18--19 February 2017, Hertie School of Governance, Berlin

17--18 October 2016, Wirtschaftsuniversität Wien

## Course Contents and Learning Objectives

R is a language and environment for statistical computing and graphics that has gained widespread acceptance in the worldwide research community. This is due to several reasons: It is an open-source solution and a comprehensive statistical software with state-of-the-art graphic capabilities and enormous flexibility. The workshop’s objective will be to teach the basic knowledge needed to use R independently, thus helping you to initiate your own process of learning the specific tools needed for your research.

The workshop takes a modern approach to learning R by emphasizing participants’ prior exposure to data analysis and statistical software. Participants will start working with datasets right away rather than creating artificial vectors and matrices. Further, the course eschews teaching R’s base graphing facilities in favor of ggplot2 -- a more intuitive solution for plotting, which produces beautiful results with little effort. The course will also introduce the "tidyverse", ""tidy data" principles and "piping", an advanced feature of the language which however makes coding more intuitive.

The workshop mixes structured lectures with practical exercises.

## General Readings

There are no required readings. If participants have the time, I strongly recommend to look at the materials of the workshop (1) or  to complete as much as possible of the online tutorials listed below (2) before attending the workshop. The other references are good books for further self-learning after the course, but having a look at them before the workshop is helpful. Participants might also want to look at the material from their methods classes in undergraduate studies or at the Hertie School, if applicable.

1. 'Introduction to R', https://gitlab.com/arndtl/r_workshop
2. 'RStudio Primers', https://rstudio.cloud/learn/primers (free interactive online exercises, no registration required)
3. Grolemund, Garret and Hadley Wickham (2017): R for Data Science. O’Reilly. (fully available online and for free at https://r4ds.had.co.nz/)
4. Wickham, Hadley (2009): ggplot2. Elegant Graphics for Data Analysis. Springer (fully available online and for free at https://ggplot2-book.org/)

## Course Structure

**Saturday**

10:00h - 11:00h Introduction to R and Rstudio

*11:00h - 11:15h break*

11:15h - 12:00h Introduction to R and RStudio (continued)

*12:00h - 13:00h Lunch break*

13:00h - 14:30h Data Manipulation

*14:30h - 15:00h coffee break*

15:00h - 16:30h Data Wrangling

**Sunday**

10:00h - 11:00h Recap and Model Estimation

*11:00h - 11:15h break*

11:15h - 12:00h Data Visualization

*12:00h - 13:00h Lunch break*

13:00h - 14:30h Data and Model Visualization

*14:30h - 15:00h coffee break*

15:00h - 16:30h RMarkdown and Wrap-up

## Course materials

[Clone](https://gitlab.com/arndtl/r_workshop.git) or download the repository (use the button styled as down-ward pointing arrow).

Slides available [here](https://aleininger.eu/r_workshop/).

## Cheat sheets

[Workshop Cheatsheet](https://gitlab.com/arndtl/r_workshop/raw/hsog/r_workshop_cheatsheet.pdf)

[Base R](http://github.com/rstudio/cheatsheets/raw/master/base-r.pdf)

[RStudio IDE](https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf)

[Data Visualization with ggplot2](https://github.com/rstudio/cheatsheets/raw/master/data-visualization-2.1.pdf)

[Data Transformation with dplyr](https://github.com/rstudio/cheatsheets/blob/master/data-transformation.pdf)


[Colors in R](http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf)

[R Reference Card](ftp://cran.r-project.org/pub/R/doc/contrib/Short-refcard.pdf)

[Built-in Functions ](http://www.statmethods.net/management/functions.html)

[Model formulas in R](https://ww2.coastal.edu/kingw/statistics/R-tutorials/formulae.html)

Some cheat sheets can also be found through RStudio: Help > Cheatsheets.

Find more at [rstudio.com/resources/cheatsheets/](https://www.rstudio.com/resources/cheatsheets/)