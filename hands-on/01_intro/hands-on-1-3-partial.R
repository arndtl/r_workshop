# Introduction to "R"
# Introduction to R and RStudio
#
# Hands-on 1-3

# This script contains the solutions to the exercise but the code contains
# deliberate omissions (denoted by __) and errors. You have fill in or fix them
# to make the code run through.

# 1 Open the file BundestagForecastReplicationData.csv and assign its content to
# an object.
d <- read.__(__)

# 2 How many observations are in the dataset?
nrow(_)

# 3 How many variables does the dataset contain?
__(d)

# 4 Create a variable of type logical which is TRUE for each election after 1990
# and FALSE for all others. Simply checke whether year is greater or equal to
# 1990 and write the result of that comparison in a new variable.
d$post1990 <- __ >= __

# 5 Calculate the mean of the variable outgovshare.
mean(d$outgovshare, __)

# 6 Remove the logical variable indicating the post-1990 elections.
d$post1990 <- NA

# 7 Z-Tranform the variable outgovshare. The function sd() calculates the
# standard deviation of a variable which you need for z-transformation.
# Remember: z = (x - mean(x))/sd(x)
df$outgovshare <- (df$outgovshare - mean(df$outgovshare na.rm = T) /
  sd(df$outgovshare, na.rm = T)

# 8 Save the data in a separate file.
__(__, 'BundestagForecastReplicationData_edit.csv', row.names = F)