# Introduction to "R"
# Data and model visualisation
#
# Hands-on 5-1

# 1. Load ggplot2 or tidyverse (which includes ggplot2).

# 2. Read the file btw2013.csv and assign it to an object.

# 3. Create a scatterplot of turnout (on the y-axis) against unemployment (on
# the x-axis).

# 4. Label the axes.

# 5. Highlight east and west germany by changing the color or shape of the dots.
# Check out scale_color_discrete() or scale_shape_discrete() for modifying the legend.

# 6. Include two black linear fit lines (with out confidence intervals), one for
# east, one for west. Check the help for geom_smooth to find out how to surpress
# confidence intervals.

# 7. Optional: Instead of separating dots and lines by east vs. west, try to
# facet the plot by federal state. Read the help file for the function
# facet_wrap() to learn more.

# 8. Optional: You notice a spelling mistake: "Schleswig-Holsten". The correct
# spelling is "Schleswig-Holstein". Correct this mistake.