# Introduction to "R"
# Data and model visualisation
#
# Hands-on 5-2

# 1. Read the data and assign it to an object.
b <- read.csv('bowleretal2016_tab1.csv')

# 2. Create the figure that you think is appropriate.