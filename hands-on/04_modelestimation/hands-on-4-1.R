# Introduction to "R"
# Model estimation
#
# Hands-on 4-1

# 1. Read the dataset "EUsuppDK.dta" and assign it to an object. You'll need the
# function read.dta() from the package foreign.

# 2. Regress eusupp on gender, wealth and age.

# 3. Interpret the coefficients. For this obtain the summary() of the model results.

# 4. Obtain the predicted values for the following model: eusupp ~ sex + wealth + age.
# Save them to an object yhats.

# 5.Then also create a variable containing the predicted values in the original
# data.frame. Hint: simply assing the object yhats won't work.

# 6. If you use the model to predict eusupp. How far off are your predictions
# on average?

# 7. How supportive of the EU do you expect a very rich 50 year old woman to be
# based on your regression model? Hint: Construct a new dataset using the
# data.frame() function and feed it to lm using the data argument.